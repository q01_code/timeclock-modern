;;; timeclock.el --- mode for keeping track of time  -*- lexical-binding:t -*-

;; Copyright (C) 1999-2023 Free Software Foundation, Inc.

;; Author: John Wiegley <johnw@gnu.org>
;; Created: 25 Mar 1999
;; Old-Version: 2.6.1
;; Keywords: calendar data

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This mode is for keeping track of time intervals.  You can use it
;; for whatever purpose you like, but the typical scenario is to keep
;; track of how much time you spend working on certain projects.
;;
;; Use `timeclock-in' when you start on a project, and `timeclock-out'
;; when you're done.  Once you've collected some data, you can use
;; `timeclock-workday-remaining' to see how much time is left to be
;; worked today (where `timeclock-workday' specifies the length of the
;; working day), and `timeclock-when-to-leave' to calculate when you're free.

;; You'll probably want to bind the timeclock commands to some handy
;; keystrokes.  Assuming C-c t is unbound, you might use:
;;
;;   (define-key (kbd "C-c t i") 'timeclock-in)
;;   (define-key (kbd "C-c t o") 'timeclock-out)
;;   (define-key (kbd "C-c t c") 'timeclock-change)
;;   (define-key (kbd "C-c t r") 'timeclock-reread-log)
;;   (define-key (kbd "C-c t u") 'timeclock-update-mode-line)
;;   (define-key (kbd "C-c t w") 'timeclock-when-to-leave-string)

;; If you want Emacs to display the amount of time "left" to your
;; workday in the mode-line, you can either set the value of
;; `timeclock-mode-line-display' to t using M-x customize, or you can
;; add this code to your init file:
;;
;;   (require 'timeclock)
;;   (timeclock-mode-line-display)
;;
;; To cancel this mode line display at any time, just call
;; `timeclock-mode-line-display' again.

;; If you are interested in tracking everything you do in a daily
;; basis, you should set the value of `timeclock-modern' to t.  It
;; will allow you to add tags to your projects by calling
;; `timeclock-in' with a prefix argument.  That way you can, for
;; instance, track which books do you read and when.  Be aware that
;; this is not compatible with the work related commands.

;; You may also want Emacs to ask you before exiting, if you are
;; currently working on a project.  This can be done either by setting
;; `timeclock-ask-before-exiting' to t using M-x customize (this is
;; the default), or by adding the following to your init file:
;;
;;   (add-hook 'kill-emacs-query-functions #'timeclock-query-out)

;; NOTE: If you change your timelog file without using timeclock's
;; functions, or if you change the value of any of timeclock's
;; customizable variables, you should run the command
;; `timeclock-reread-log'.  This will recompute any discrepancies in
;; your average working time, and will make sure that the various
;; display functions return the correct value.

;;; Code:

(require 'calendar)
(require 'cl-lib)
(require 'rx)
(require 'time-date)
(require 'transient)

(defgroup timeclock nil
  "Keeping track of the time that gets spent."
  :group 'data)

;;; User Variables:

(defcustom timeclock-file (locate-user-emacs-file "timelog" ".timelog")
  "The file used to store timeclock data in."
  :version "24.4"			; added locate-user-emacs-file
  :type 'file)

(defcustom timeclock-workday (* 8 60 60)
  "The length of a work period in seconds."
  :type 'natnum)

(defcustom timeclock-modern nil
  "Whether to make timeclock track everything.
When non-nil, timeclock will behave more similarly to modern time
trackers, which are not limited to tracking work.

The following things will change once this variable is set:

- Tracks will optionally have a tag attached in addition to a
  project.  The user is expected to give projects (or \"items\",
  which better suits this workflow) general names such as
  \"Read\", so tags can be be used to fine-grain on a per-track
  basic.  For instance, a good tag for the item \"Read\" would be
  \"Don Quixote\", or any book the user is reading.
- `timeclock-in' will prompt for a tag when called with a prefix
  argument.  Also, it will work even if the last track wasn't
  clocked-out.
- Statistics will be more detailed and will offer tag reports as
  well.
- All features related with work time limits will be ignored."
  :type 'boolean)

(defcustom timeclock-separator "─────>"
  "Separator used for timeclock's statistic report buffers."
  :type 'string)

(defvar timeclock--previous-workday nil)

(defcustom timeclock-relative t
  "Whether to make reported time relative to `timeclock-workday'.
For example, if the length of a normal workday is eight hours, and you
work four hours on Monday, then the amount of time \"remaining\" on
Tuesday is twelve hours -- relative to an averaged work period of
eight hours -- or eight hours, non-relative.  So relative time takes
into account any discrepancy of time under-worked or over-worked on
previous days.  This only affects the timeclock mode line display."
  :type 'boolean)

(defcustom timeclock-get-project-function 'timeclock-ask-for-project
  "The function used to determine the name of the current project.
When clocking in, and no project is specified, this function will be
called to determine what is the current project to be worked on.
If this variable is nil, no questions will be asked."
  :type 'function)

(defcustom timeclock-get-reason-function 'timeclock-ask-for-reason
  "A function used to determine the reason for clocking out.
When clocking out, and no reason is specified, this function will be
called to determine what is the reason.
If this variable is nil, no questions will be asked."
  :type 'function)

(defcustom timeclock-get-workday-function nil
  "A function used to determine the length of today's workday.
The first time that a user clocks in each day, this function will be
called to determine what is the length of the current workday.  If
the return value is nil, or equal to `timeclock-workday', nothing special
will be done.  If it is a quantity different from `timeclock-workday',
however, a record will be output to the timelog file to note the fact that
that day has a length that is different from the norm."
  :type '(choice (const nil) function))

(defcustom timeclock-ask-before-exiting t
  "If non-nil, ask if the user wants to clock out before exiting Emacs.
This variable only has effect if set with \\[customize]."
  :set (lambda (symbol value)
	 (if value
	     (add-hook 'kill-emacs-query-functions #'timeclock-query-out)
	   (remove-hook 'kill-emacs-query-functions #'timeclock-query-out))
	 (set symbol value))
  :type 'boolean)

(defvar timeclock-update-timer nil
  "The timer used to update `timeclock-mode-string'.")

;; For byte-compiler.
(defvar display-time-hook)
(defvar timeclock-mode-line-display)

(defcustom timeclock-use-display-time t
  "If non-nil, use `display-time-hook' for doing mode line updates.
The advantage of this is that one less timer has to be set running
amok in Emacs's process space.  The disadvantage is that it requires
you to have `display-time' running.  If you don't want to use
`display-time', but still want the mode line to show how much time is
left, set this variable to nil.  Changing the value of this variable
while timeclock information is being displayed in the mode line has no
effect.  You should call the function `timeclock-mode-line-display' with
a positive argument to force an update."
  :set (lambda (symbol value)
	 (let ((currently-displaying
		(and (boundp 'timeclock-mode-line-display)
		     timeclock-mode-line-display)))
	   ;; if we're changing to the state that
	   ;; `timeclock-mode-line-display' is already using, don't
	   ;; bother toggling it.  This happens on the initial loading
	   ;; of timeclock.el.
	   (if (and currently-displaying
		    (or (and value
			     (boundp 'display-time-hook)
			     (memq #'timeclock-update-mode-line
				   display-time-hook))
			(and (not value)
			     timeclock-update-timer)))
	       (setq currently-displaying nil))
	   (and currently-displaying
		(setq timeclock-mode-line-display nil))
	   (set symbol value)
	   (and currently-displaying
		(setq timeclock-mode-line-display t))
           ;; FIXME: The return value isn't used, AFAIK!
	   value))
  :type 'boolean
  :require 'time)

(defcustom timeclock-first-in-hook nil
  "A hook run for the first \"in\" event each day.
Note that this hook is run before recording any events.  Thus the
value of `timeclock-hours-today', `timeclock-last-event' and the
return value of function `timeclock-last-period' are relative previous
to today."
  :type 'hook)

(defcustom timeclock-load-hook nil
  "Hook that gets run after timeclock has been loaded."
  :type 'hook)
(make-obsolete-variable 'timeclock-load-hook
                        "use `with-eval-after-load' instead." "28.1")

(defcustom timeclock-in-hook nil
  "A hook run every time an \"in\" event is recorded."
  :type 'hook)

(defcustom timeclock-day-over-hook nil
  "A hook that is run when the workday has been completed.
This hook is only run if the current time remaining is being displayed
in the mode line.  See the variable `timeclock-mode-line-display'."
  :type 'hook)

(defcustom timeclock-out-hook nil
  "A hook run every time an \"out\" event is recorded."
  :type 'hook)

(defcustom timeclock-done-hook nil
  "A hook run every time a project is marked as completed."
  :type 'hook)

(defcustom timeclock-event-hook nil
  "A hook run every time any event is recorded."
  :type 'hook)

(defvar timeclock-last-event nil
  "A list containing the last event that was recorded.
The format of this list is (CODE TIME PROJECT).")

(defvar timeclock-last-event-workday nil
  "The number of seconds in the workday of `timeclock-last-event'.")

;;; Internal Variables:

(defvar timeclock-discrepancy nil
  "A variable containing the time discrepancy before the last event.
Normally, timeclock assumes that you intend to work for
`timeclock-workday' seconds every day.  Any days in which you work
more or less than this amount is considered either a positive or
a negative discrepancy.  If you work in such a manner that the
discrepancy is always brought back to zero, then you will by
definition have worked an average amount equal to `timeclock-workday'
each day.")

(defvar timeclock-elapsed nil
  "A variable containing the time elapsed for complete periods today.
This value is not accurate enough to be useful by itself.  Rather,
call `timeclock-workday-elapsed', to determine how much time has been
worked so far today.  Also, if `timeclock-relative' is nil, this value
will be the same as `timeclock-discrepancy'.")

(defvar timeclock-use-elapsed nil
  "Non-nil if the mode line should display time elapsed, not remaining.")

(defvar timeclock-last-period nil
  "Integer representing the number of seconds in the last period.
Note that you shouldn't access this value, but instead should use the
function `timeclock-last-period'.")

(defvar timeclock-mode-string nil
  "The timeclock string (optionally) displayed in the mode line.
The time is bracketed by <> if you are clocked in, otherwise by [].")

(defvar timeclock-day-over nil
  "The date of the last day when notified \"day over\" for.")

;;; User Functions:

;;;###autoload
(define-minor-mode timeclock-mode-line-display
  "Toggle display of the amount of time left today in the mode line.
If `timeclock-use-display-time' is non-nil (the default), then
the function `display-time-mode' must be active, and the mode line
will be updated whenever the time display is updated.  Otherwise,
the timeclock will use its own sixty second timer to do its
updating.  With prefix ARG, turn mode line display on if and only
if ARG is positive.  Returns the new status of timeclock mode line
display (non-nil means on).

If using a customized `timeclock-workday' value, this should be
set before switching this mode on."
  :global t
  ;; cf display-time-mode.
  (setq timeclock-mode-string "")
  (or global-mode-string (setq global-mode-string '("")))
  (if timeclock-mode-line-display
      (progn
        (or (memq 'timeclock-mode-string global-mode-string)
            (setq global-mode-string
                  (append global-mode-string '(timeclock-mode-string))))
        (add-hook 'timeclock-event-hook #'timeclock-update-mode-line)
        (when timeclock-update-timer
          (cancel-timer timeclock-update-timer)
          (setq timeclock-update-timer nil))
        (if (boundp 'display-time-hook)
            (remove-hook 'display-time-hook #'timeclock-update-mode-line))
        (if timeclock-use-display-time
            (progn
              ;; Update immediately so there is a visible change
              ;; on calling this function.
              (if display-time-mode
                  (timeclock-update-mode-line)
                (message "Activate `display-time-mode' or turn off \
`timeclock-use-display-time' to see timeclock information"))
              (add-hook 'display-time-hook #'timeclock-update-mode-line))
          (setq timeclock-update-timer
                (run-at-time nil 60 'timeclock-update-mode-line))))
    (setq global-mode-string
          (delq 'timeclock-mode-string global-mode-string))
    (remove-hook 'timeclock-event-hook #'timeclock-update-mode-line)
    (if (boundp 'display-time-hook)
        (remove-hook 'display-time-hook
                     #'timeclock-update-mode-line))
    (when timeclock-update-timer
      (cancel-timer timeclock-update-timer)
      (setq timeclock-update-timer nil))))

(defsubst timeclock-time-to-date (&optional time)
  "Convert the TIME value to a textual date string."
  (format-time-string "%Y/%m/%d" time))

(defun timeclock--add-track (item &optional tag)
  "Like `timeclock-in', but only accepts project and tag.
ITEM is a project and TAG is a tag to add.  Both must be strings.
This funtion assumes `timeclock-modern' is non-nil."
  (when timeclock-modern
    ;; As this function only makes sense when `timeclock-modern' is
    ;; non-nil, we don't do anything if that's not the case.
    (timeclock-in tag item nil tag)))

;;;###autoload
(defun timeclock-in (&optional arg project find-project tag)
  "Clock in, recording the current time moment in the timelog.
With a numeric prefix ARG, record the fact that today has only that
many hours in it to be worked.  If ARG is a non-numeric prefix argument
\(non-nil, but not a number), 0 is assumed (working on a holiday or
weekend).  *If not called interactively, ARG should be the number of
_seconds_ worked today*.  This feature only has effect the first time
this function is called within a day.  When `timeclock-modern' is
non-nil, prefix argument is instead used to know whether to log a tag
alongside project.

PROJECT is the project being clocked into.  If PROJECT is nil, and
FIND-PROJECT is non-nil -- or the user calls `timeclock-in'
interactively -- call the function `timeclock-get-project-function' to
discover the name of the project.  However, when `timeclock-modern' is
non-nil, FIND-PROJECT has no effect, as in that case `timeclock--complete'
is used instead.

TAG indicates a tag to add alongside track.  When `timeclock-modern'
is nil, this is ignored."
  (interactive
   (list (and current-prefix-arg
	      (if (numberp current-prefix-arg)
		  (* current-prefix-arg 60 60)
		0))))
  (if (and (equal (car timeclock-last-event) "i") (not timeclock-modern))
      (error "You've already clocked in!")
    (unless timeclock-last-event
      (timeclock-reread-log))
    ;; Either no log file, or day has rolled over.
    (unless (or (and timeclock-last-event
                     (equal (timeclock-time-to-date
                             (cadr timeclock-last-event))
                            (timeclock-time-to-date)))
                ;; timeclock-modern users don't want to track work this way; if
                ;; they do, they shouldn't use timeclock-modern to begin with.
                (not timeclock-modern))
      (let ((workday (or (and (numberp arg) arg)
			 (and arg 0)
			 (and timeclock-get-workday-function
			      (funcall timeclock-get-workday-function))
			 timeclock-workday)))
	(run-hooks 'timeclock-first-in-hook)
	;; settle the discrepancy for the new day
	(setq timeclock-discrepancy
	      (- (or timeclock-discrepancy 0) workday))
	(if (not (= workday timeclock-workday))
	    (timeclock-log "h" (number-to-string
				(/ workday (if (zerop (% workday (* 60 60)))
					       60 60.0)
                                   60))))))
    (if timeclock-modern
        (timeclock-log
         " "
         (or project
             (setq project (timeclock--complete "Start clocking: " 'items)))
         (when arg
           (or tag (timeclock--complete "Tag: " project))))
      (timeclock-log "i" (or project
			     (and timeclock-get-project-function
				  (or find-project
				      (called-interactively-p 'interactive))
				  (funcall timeclock-get-project-function)))))
    (run-hooks 'timeclock-in-hook)
    (when timeclock-modern
      (message "Clocking \"%s\"" project))))

;;;###autoload
(defun timeclock-out (&optional arg reason find-reason)
  "Clock out, recording the current time moment in the timelog.
If a prefix ARG is given, the user has completed the project that was
begun during the last time segment.

REASON is the user's reason for clocking out.  If REASON is nil, and
FIND-REASON is non-nil -- or the user calls `timeclock-out'
interactively -- call the function `timeclock-get-reason-function' to
discover the reason."
  (interactive "P")
  (or timeclock-last-event timeclock-modern
      (error "You haven't clocked in!"))
  (if (equal (downcase (car timeclock-last-event)) "o")
      (error "You've already clocked out!")
    (timeclock-log
     (if arg "O" "o")
     (or reason
	 (and timeclock-get-reason-function
	      (or find-reason (called-interactively-p 'interactive))
	      (funcall timeclock-get-reason-function))))
    (run-hooks 'timeclock-out-hook)
    (if arg
	(run-hooks 'timeclock-done-hook))))

;; Should today-only be removed in favor of timeclock-relative? - gm
(defsubst timeclock-workday-remaining (&optional today-only)
  "Return the number of seconds until the workday is complete.
The amount returned is relative to the value of `timeclock-workday'.
If TODAY-ONLY is non-nil, the value returned will be relative only to
the time worked today, and not to past time."
  (let ((discrep (timeclock-find-discrep)))
    (if discrep
        (- (if today-only (cadr discrep)
             (car discrep)))
      0.0)))

;;;###autoload
(defun timeclock-status-string (&optional show-seconds today-only)
  "Report the overall timeclock status at the present moment.
If SHOW-SECONDS is non-nil, display second resolution.
If TODAY-ONLY is non-nil, the display will be relative only to time
worked today, ignoring the time worked on previous days."
  (interactive "P")
  (let* ((remainder (timeclock-workday-remaining
		     (or today-only
			 (not timeclock-relative))))
         (last-in (equal (car timeclock-last-event) "i"))
         (status
	  (format "Currently %s since %s (%s), %s %s, leave at %s"
		  (if last-in "IN" "OUT")
		  (if show-seconds
		      (format-time-string "%-I:%M:%S %p"
					  (nth 1 timeclock-last-event))
		    (format-time-string "%-I:%M %p"
					(nth 1 timeclock-last-event)))
		  (or (nth 2 timeclock-last-event)
		      (if last-in "**UNKNOWN**" "workday over"))
		  (timeclock-seconds-to-string remainder show-seconds t)
		  (if (> remainder 0)
		      "remaining" "over")
		  (timeclock-when-to-leave-string show-seconds today-only))))
    (if (called-interactively-p 'interactive)
	(message "%s" status)
      status)))

;;;###autoload
(defun timeclock-change (&optional arg project)
  "Change to working on a different project.
This clocks out of the current project, then clocks in on a new one.
With a prefix ARG, consider the previous project as finished at the
time of changeover.  PROJECT is the name of the last project you were
working on."
  (interactive "P")
  (timeclock-out arg)
  (timeclock-in nil project (called-interactively-p 'interactive)))

;;;###autoload
(defun timeclock-query-out ()
  "Ask the user whether to clock out.
This is a useful function for adding to `kill-emacs-query-functions'."
  (and (equal (car timeclock-last-event) "i")
       (y-or-n-p "You're currently clocking time, clock out? ")
       (timeclock-out))
  ;; Unconditionally return t for `kill-emacs-query-functions'.
  t)

;;;###autoload
(defun timeclock-reread-log ()
  "Re-read the timeclock, to account for external changes.
Returns the new value of `timeclock-discrepancy'."
  (interactive)
  (setq timeclock-discrepancy nil)
  (timeclock-find-discrep)
  (if (and timeclock-discrepancy timeclock-mode-line-display)
      (timeclock-update-mode-line))
  timeclock-discrepancy)

(defun timeclock-seconds-to-string (seconds &optional show-seconds
					    reverse-leader)
  "Convert SECONDS into a compact time string.
If SHOW-SECONDS is non-nil, make the resolution of the return string
include the second count.  If REVERSE-LEADER is non-nil, it means to
output a \"+\" if the time value is negative, rather than a \"-\".
This is used when negative time values have an inverted meaning (such
as with time remaining, where negative time really means overtime)."
  (let ((s (abs (truncate seconds))))
    (format (if show-seconds "%s%d:%02d:%02d" "%s%d:%02d")
	    (if (< seconds 0) (if reverse-leader "+" "-") "")
	    (/ s 3600) (% (/ s 60) 60) (% s 60))))

(defsubst timeclock-currently-in-p ()
  "Return non-nil if the user is currently clocked in."
  (equal (car timeclock-last-event) "i"))

;;;###autoload
(defun timeclock-workday-remaining-string (&optional show-seconds
						     today-only)
  "Return a string representing the amount of time left today.
Display second resolution if SHOW-SECONDS is non-nil.  If TODAY-ONLY
is non-nil, the display will be relative only to time worked today.
See `timeclock-relative' for more information about the meaning of
\"relative to today\"."
  (interactive)
  (let ((string (timeclock-seconds-to-string
		 (timeclock-workday-remaining today-only)
		 show-seconds t)))
    (if (called-interactively-p 'interactive)
	(message "%s" string)
      string)))

(defsubst timeclock-workday-elapsed ()
  "Return the number of seconds worked so far today.
If RELATIVE is non-nil, the amount returned will be relative to past
time worked.  The default is to return only the time that has elapsed
so far today."
  (let ((discrep (timeclock-find-discrep)))
    (if discrep
	(nth 2 discrep)
      0.0)))

;;;###autoload
(defun timeclock-workday-elapsed-string (&optional show-seconds)
  "Return a string representing the amount of time worked today.
Display seconds resolution if SHOW-SECONDS is non-nil.  If RELATIVE is
non-nil, the amount returned will be relative to past time worked."
  (interactive)
  (let ((string (timeclock-seconds-to-string (timeclock-workday-elapsed)
					     show-seconds)))
    (if (called-interactively-p 'interactive)
	(message "%s" string)
      string)))

(define-obsolete-function-alias 'timeclock-time-to-seconds 'float-time "26.1")
(define-obsolete-function-alias 'timeclock-seconds-to-time 'time-convert "26.1")

;; Should today-only be removed in favor of timeclock-relative? - gm
(defsubst timeclock-when-to-leave (&optional today-only)
  "Return a time value representing the end of today's workday.
If TODAY-ONLY is non-nil, the value returned will be relative only to
the time worked today, and not to past time."
  (time-since (let ((discrep (timeclock-find-discrep)))
		(if discrep
		    (if today-only
			(cadr discrep)
		      (car discrep))
		  0))))

;;;###autoload
(defun timeclock-when-to-leave-string (&optional show-seconds
						 today-only)
  "Return a string representing the end of today's workday.
This string is relative to the value of `timeclock-workday'.  If
SHOW-SECONDS is non-nil, the value printed/returned will include
seconds.  If TODAY-ONLY is non-nil, the value returned will be
relative only to the time worked today, and not to past time."
  ;; Should today-only be removed in favor of timeclock-relative? - gm
  (interactive)
  (let* ((then (timeclock-when-to-leave today-only))
	 (string
	  (if show-seconds
	      (format-time-string "%-I:%M:%S %p" then)
	    (format-time-string "%-I:%M %p" then))))
    (if (called-interactively-p 'interactive)
	(message "%s" string)
      string)))

(defun timeclock-make-hours-explicit (old-default)
  "Specify all workday lengths in `timeclock-file'.
OLD-DEFAULT hours are set for every day that has no number indicated."
  (interactive "P")
  (if old-default (setq old-default (prefix-numeric-value old-default))
    (error "`timeclock-make-hours-explicit' requires an explicit argument"))
  (let ((extant-timelog (find-buffer-visiting timeclock-file))
	current-date)
    (with-current-buffer (find-file-noselect timeclock-file t)
      (unwind-protect
	  (save-excursion
	    (save-restriction
	      (widen)
	      (goto-char (point-min))
	      (while (progn (skip-chars-forward "\n") (not (eobp)))
		;; This is just a variant of `timeclock-moment-regexp'.
		(unless (looking-at
			 (concat "^\\([bhioO]\\) \\([0-9]+/[0-9]+/[0-9]+\\) "
				 "\\([0-9]+:[0-9]+:[0-9]+\\)"))
		  (error "Can't parse `%s'" timeclock-file))
		(let ((this-date (match-string 2)))
		  (unless (or (and current-date
				   (string= this-date current-date))
			      (string= (match-string 1) "h"))
		    (insert (format "h %s %s %s\n" (match-string 2)
				    (match-string 3) old-default)))
		  (if (string-match "^[ih]" (match-string 1)) ; ignore logouts
		      (setq current-date this-date)))
		(forward-line))
	      (save-buffer)))
	(unless extant-timelog (kill-buffer (current-buffer)))))))

;;; Internal Functions:

(defvar timeclock-project-list nil)
(defvar timeclock-last-project nil)

(defun timeclock-completing-read (prompt alist &optional default)
  "A version of `completing-read'.
PROMPT, ALIST and DEFAULT are used for the PROMPT, COLLECTION and DEF
arguments of `completing-read'."
  (declare (obsolete completing-read "27.1"))
  (completing-read prompt alist nil nil nil nil default))

(defun timeclock-ask-for-project ()
  "Ask the user for the project they are clocking into."
  (completing-read
   (format-prompt "Clock into which project"
	          (or timeclock-last-project
	              (car timeclock-project-list)))
   timeclock-project-list
   nil nil nil nil
   (or timeclock-last-project
       (car timeclock-project-list))))

(defvar timeclock-reason-list nil)

(defun timeclock-ask-for-reason ()
  "Ask the user for the reason they are clocking out."
  (completing-read "Reason for clocking out: " timeclock-reason-list))

(defun timeclock-update-mode-line ()
  "Update the `timeclock-mode-string' displayed in the mode line.
The value of `timeclock-relative' affects the display as described in
that variable's documentation."
  (interactive)
  (let ((remainder
	 (if timeclock-use-elapsed
	     (timeclock-workday-elapsed)
	   (timeclock-workday-remaining (not timeclock-relative))))
	(last-in (equal (car timeclock-last-event) "i"))
	(todays-date (timeclock-time-to-date)))
    (when (and (< remainder 0)
	       (not (and timeclock-day-over
			 (equal timeclock-day-over todays-date))))
      (setq timeclock-day-over todays-date)
      (run-hooks 'timeclock-day-over-hook))
    (setq timeclock-mode-string
          (propertize
           (format " %c%s%c "
                   (if last-in ?< ?\[)
                   (timeclock-seconds-to-string remainder nil t)
		   (if last-in ?> ?\]))
           'help-echo "timeclock: time remaining"))))

(put 'timeclock-mode-string 'risky-local-variable t)

(defun timeclock-log (code &optional project tag)
  "Log the event CODE to the timeclock log, at the time of call.
If PROJECT is a string, it represents the project which the event is
being logged for.  Normally only \"in\" events specify a project.
TAG (a string) is added immediately after PROJECT, within brackets."
  (let ((extant-timelog (find-buffer-visiting timeclock-file)))
    (with-current-buffer (find-file-noselect timeclock-file t)
      (save-excursion
	(save-restriction
	  (widen)
	  (goto-char (point-max))
	  (if (not (bolp))
	      (insert "\n"))
	  (let ((now (current-time)))
	    (insert code " "
		    (format-time-string "%Y/%m/%d %H:%M:%S" now)
		    (or (and (stringp project)
			     (> (length project) 0)
			     (concat " " project))
			"")
                    (or (and (stringp tag)
                             (> (length tag) 0)
                             (concat " [" tag "]"))
                        "")
		    "\n")
	    (if (equal (downcase code) "o")
		(setq timeclock-last-period
		      (float-time
		       (time-subtract now (cadr timeclock-last-event)))
		      timeclock-discrepancy
		      (+ timeclock-discrepancy
			 timeclock-last-period)))
	    (setq timeclock-last-event (list code now project)))))
      (save-buffer)
      (unless extant-timelog (kill-buffer (current-buffer)))))
  (run-hooks 'timeclock-event-hook))

(defvar timeclock-moment-regexp
  (concat "\\([bhioO]\\)\\s-+"
	  "\\([0-9]+\\)/\\([0-9]+\\)/\\([0-9]+\\)\\s-+"
	  "\\([0-9]+\\):\\([0-9]+\\):\\([0-9]+\\)[ \t]*" "\\([^\n]*\\)"))

(defun timeclock-read-moment ()
  "Read the moment under point from the timelog."
  (if (looking-at timeclock-moment-regexp)
      (let ((code (match-string 1))
	    (year (string-to-number (match-string 2)))
	    (mon  (string-to-number (match-string 3)))
	    (mday (string-to-number (match-string 4)))
	    (hour (string-to-number (match-string 5)))
	    (min  (string-to-number (match-string 6)))
	    (sec  (string-to-number (match-string 7)))
	    (project (match-string 8)))
	(list code (encode-time sec min hour mday mon year) project))))

(defun timeclock-last-period (&optional moment)
  "Return the value of the last event period.
If the last event was a clock-in, the period will be open ended, and
growing every second.  Otherwise, it is a fixed amount which has been
recorded to disk.  If MOMENT is non-nil, use that as the current time.
This is only provided for coherency when used by
`timeclock-discrepancy'."
  (if (equal (car timeclock-last-event) "i")
      (float-time (time-subtract moment (cadr timeclock-last-event)))
    timeclock-last-period))

(cl-defstruct (timeclock-entry
               (:constructor nil) (:copier nil)
               (:type list))
  begin end project comment
  ;; FIXME: Documented in docstring of timeclock-log-data, but I can't see
  ;; where it's used in the code.
  final-p)

(defsubst timeclock-entry-length (entry)
  "Return the length of ENTRY in seconds."
  (float-time (time-subtract (cadr entry) (car entry))))

(defsubst timeclock-entry-list-length (entry-list)
  "Return the total length of ENTRY-LIST in seconds."
  (let ((length 0))
    (dolist (entry entry-list)
      (setq length (+ length (timeclock-entry-length entry))))
    length))

(defsubst timeclock-entry-list-begin (entry-list)
  "Return the start time of the first element of ENTRY-LIST."
  (timeclock-entry-begin (car entry-list)))

(defsubst timeclock-entry-list-end (entry-list)
  "Return the end time of the last element of ENTRY-LIST."
  (timeclock-entry-end (car (last entry-list))))

(defsubst timeclock-entry-list-span (entry-list)
  "Return the total time in seconds spanned by ENTRY-LIST."
  (float-time (time-subtract (timeclock-entry-list-end entry-list)
			     (timeclock-entry-list-begin entry-list))))

(defsubst timeclock-entry-list-break (entry-list)
  "Return the total break time (span - length) in ENTRY-LIST."
  (- (timeclock-entry-list-span entry-list)
     (timeclock-entry-list-length entry-list)))

(defun timeclock-entry-list-projects (entry-list)
  "Return a list of all the projects in ENTRY-LIST."
  (let (projects)
    (dolist (entry entry-list)
      (cl-pushnew (timeclock-entry-project entry) projects :test #'equal))
    projects))

(defsubst timeclock-day-required (day)
  "Return the required length of DAY in seconds, default `timeclock-workday'."
  (or (car day) timeclock-workday))

(defsubst timeclock-day-length (day)
  "Return the actual length of DAY in seconds."
  (timeclock-entry-list-length (cdr day)))

(defsubst timeclock-day-debt (day)
  "Return the debt (required - actual) associated with DAY, in seconds."
  (- (timeclock-day-required day)
     (timeclock-day-length day)))

(defsubst timeclock-day-begin (day)
  "Return the start time of DAY."
  (timeclock-entry-list-begin (cdr day)))

(defsubst timeclock-day-end (day)
  "Return the end time of DAY."
  (timeclock-entry-list-end (cdr day)))

(defsubst timeclock-day-span (day)
  "Return the span of DAY."
  (timeclock-entry-list-span (cdr day)))

(defsubst timeclock-day-break (day)
  "Return the total break time of DAY."
  (timeclock-entry-list-break (cdr day)))

(defsubst timeclock-day-projects (day)
  "Return a list of all the projects in DAY."
  (timeclock-entry-list-projects (cddr day)))

(defun timeclock-day-list-template (func day-list)
  "Template for summing the result of FUNC on each element of DAY-LIST."
  (let ((length 0))
    (dolist (day day-list)
      (setq length (+ length (funcall func day))))
    length))

(defun timeclock-day-list-required (day-list)
  "Return total required length of DAY-LIST, in seconds."
  (timeclock-day-list-template #'timeclock-day-required day-list))

(defun timeclock-day-list-length (day-list)
  "Return actual length of DAY-LIST, in seconds."
  (timeclock-day-list-template #'timeclock-day-length day-list))

(defun timeclock-day-list-debt (day-list)
  "Return total debt (required - actual) of DAY-LIST."
  (timeclock-day-list-template #'timeclock-day-debt day-list))

(defsubst timeclock-day-list-begin (day-list)
  "Return the start time of DAY-LIST."
  (timeclock-day-begin (car day-list)))

(defsubst timeclock-day-list-end (day-list)
  "Return the end time of DAY-LIST."
  (timeclock-day-end (car (last day-list))))

(defun timeclock-day-list-span (day-list)
  "Return the span of DAY-LIST."
  (timeclock-day-list-template #'timeclock-day-span day-list))

(defun timeclock-day-list-break (day-list)
  "Return the total break of DAY-LIST."
  (timeclock-day-list-template #'timeclock-day-break day-list))

(defun timeclock-day-list-projects (day-list)
  "Return a list of all the projects in DAY-LIST."
  (let (projects)
    (dolist (day day-list)
      (dolist (proj (timeclock-day-projects day))
        (cl-pushnew proj projects :test #'equal)))
    projects))

(defsubst timeclock-current-debt (&optional log-data)
  "Return the seconds debt from LOG-DATA, default `timeclock-log-data'."
  (nth 0 (or log-data (timeclock-log-data))))

(defsubst timeclock-day-alist (&optional log-data)
  "Return the date alist from LOG-DATA, default `timeclock-log-data'."
  (nth 1 (or log-data (timeclock-log-data))))

(defun timeclock-day-list (&optional log-data)
  "Return a list of the cdrs of the date alist from LOG-DATA."
  (let (day-list)
    (dolist (date-list (timeclock-day-alist log-data))
      (push (cdr date-list) day-list))
    day-list))

(defsubst timeclock-project-alist (&optional log-data)
  "Return the project alist from LOG-DATA, default `timeclock-log-data'."
  (nth 2 (or log-data (timeclock-log-data))))

(defun timeclock-log-data (&optional recent-only filename)
  "Return the contents of the timelog file, in a useful format.
If the optional argument RECENT-ONLY is non-nil, only show the contents
from the last point where the time debt (see below) was set.
If the optional argument FILENAME is non-nil, it is used instead of
the file specified by `timeclock-file.'

A timelog contains data in the form of a single entry per line.
Each entry has the form:

  CODE YYYY/MM/DD HH:MM:SS [COMMENT]

CODE is one of: b, h, i, o or O.  COMMENT is optional when the code is
i, o or O.  The meanings of the codes are:

  b  Set the current time balance, or \"time debt\".  Useful when
     archiving old log data, when a debt must be carried forward.
     The COMMENT here is the number of seconds of debt.

  h  Set the required working time for the given day.  This must
     be the first entry for that day.  The COMMENT in this case is
     the number of hours in this workday.  Floating point amounts
     are allowed.

  i  Clock in.  The COMMENT in this case should be the name of the
     project worked on.

  o  Clock out.  COMMENT is unnecessary, but can be used to provide
     a description of how the period went, for example.

  O  Final clock out.  Whatever project was being worked on, it is
     now finished.  Useful for creating summary reports.

When this function is called, it will return a data structure with the
following format:

  (DEBT ENTRIES-BY-DAY ENTRIES-BY-PROJECT)

DEBT is a floating point number representing the number of seconds
“owed” before any work was done.  For a new file (one without a `b'
entry), this is always zero.

The two entries lists have similar formats.  They are both alists,
where the CAR is the index, and the CDR is a list of time entries.
For ENTRIES-BY-DAY, the CAR is a textual date string, of the form
YYYY/MM/DD.  For ENTRIES-BY-PROJECT, it is the name of the project
worked on, or t for the default project.

The CDR for ENTRIES-BY-DAY is slightly different than for
ENTRIES-BY-PROJECT.  It has the following form:

  (DAY-LENGTH TIME-ENTRIES...)

For ENTRIES-BY-PROJECT, there is no DAY-LENGTH member.  It is simply a
list of TIME-ENTRIES.  Note that if DAY-LENGTH is nil, it means
whatever is the default should be used.

A TIME-ENTRY is a recorded time interval.  It has the following format
\(although generally one does not have to manipulate these entries
directly; see below):

  (BEGIN-TIME END-TIME PROJECT [COMMENT] [FINAL-P])

Anyway, suffice it to say there are a lot of structures.  Typically
the user is expected to manipulate to the day(s) or project(s) that he
or she wants, at which point the following helper functions may be
used:

  timeclock-day-required
  timeclock-day-length
  timeclock-day-debt
  timeclock-day-begin
  timeclock-day-end
  timeclock-day-span
  timeclock-day-break
  timeclock-day-projects

  timeclock-day-list-required
  timeclock-day-list-length
  timeclock-day-list-debt
  timeclock-day-list-begin
  timeclock-day-list-end
  timeclock-day-list-span
  timeclock-day-list-break
  timeclock-day-list-projects

  timeclock-entry-length
  timeclock-entry-begin
  timeclock-entry-end
  timeclock-entry-project
  timeclock-entry-comment

  timeclock-entry-list-length
  timeclock-entry-list-begin
  timeclock-entry-list-end
  timeclock-entry-list-span
  timeclock-entry-list-break
  timeclock-entry-list-projects

A few comments should make the use of the above functions obvious:

  `required' is the amount of time that must be spent during a day, or
  sequence of days, in order to have no debt.

  `length' is the actual amount of time that was spent.

  `debt' is the difference between required time and length.  A
  negative debt signifies overtime.

  `begin' is the earliest moment at which work began.

  `end' is the final moment work was done.

  `span' is the difference between begin and end.

  `break' is the difference between span and length.

  `project' is the project that was worked on, and `projects' is a
  list of all the projects that were worked on during a given period.

  `comment', where it applies, could mean anything.

There are a few more functions available, for locating day and entry
lists:

  timeclock-day-alist LOG-DATA
  timeclock-project-alist LOG-DATA
  timeclock-current-debt LOG-DATA

See the documentation for the given function if more info is needed."
  (let ((log-data (list 0.0 nil nil))
	(now (current-time))
	last-date-limited last-date-seconds last-date
	(line 0) last beg day entry event)
    (with-temp-buffer
      (insert-file-contents (or filename timeclock-file))
      (when recent-only
	(goto-char (point-max))
	(unless (re-search-backward "^b\\s-+" nil t)
	  (goto-char (point-min))))
      (while (or (setq event (timeclock-read-moment))
		 (and beg (not last)
		      (setq last t event (list "o" now))))
	(setq line (1+ line))
	(pcase (car event)
          ("b"
	   (setcar log-data (string-to-number (nth 2 event))))
	  ("h"
	   (setq last-date-limited (timeclock-time-to-date (cadr event))
		 last-date-seconds (* (string-to-number (nth 2 event))
				      3600.0)))
	  ("i"
	   (if beg
	       (error "Error in format of timelog file, line %d" line)
	     (setq beg t))
	   (setq entry (list (cadr event) nil
			     (and (> (length (nth 2 event)) 0)
				  (nth 2 event))))
	   (let ((date (timeclock-time-to-date (cadr event))))
	     (if (and last-date
		      (not (equal date last-date)))
		 (progn
		   (setcar (cdr log-data)
			   (cons (cons last-date day)
				 (cadr log-data)))
		   (setq day (list (and last-date-limited
					last-date-seconds))))
	       (unless day
		 (setq day (list (and last-date-limited
				      last-date-seconds)))))
	     (setq last-date date
		   last-date-limited nil)))
	  ((or "o" "O")
	   (if (not beg)
	       (error "Error in format of timelog file, line %d" line)
	     (setq beg nil))
	   (setcar (cdr entry) (cadr event))
	   (let ((desc (and (> (length (nth 2 event)) 0)
			    (nth 2 event))))
	     (if desc
		 (nconc entry (list (nth 2 event))))
	     (if (equal (car event) "O")
		 (nconc entry (if desc
				  (list t)
				(list nil t))))
	     (nconc day (list entry))
	     (setq desc (nth 2 entry))
	     (let ((proj (assoc desc (nth 2 log-data))))
	       (if (null proj)
		   (setcar (cddr log-data)
			   (cons (cons desc (list entry))
				 (nth 2 log-data)))
		 (nconc (cdr proj) (list entry)))))))
	(forward-line))
      (if day
	  (setcar (cdr log-data)
		  (cons (cons last-date day)
			(cadr log-data))))
      log-data)))

(defun timeclock-find-discrep ()
  "Calculate time discrepancies, in seconds.
The result is a three element list, containing the total time
discrepancy, today's discrepancy, and the time worked today."
  ;; This is not implemented in terms of the functions above, because
  ;; it's a bit wasteful to read all of that data in, just to throw
  ;; away more than 90% of the information afterwards.
  ;;
  ;; If it were implemented using those functions, it would look
  ;; something like this:
  ;;  (let ((days (timeclock-day-alist (timeclock-log-data)))
  ;;        (total 0.0))
  ;;    (while days
  ;;      (setq total (+ total (- (timeclock-day-length (cdar days))
  ;;                              (timeclock-day-required (cdar days))))
  ;;            days (cdr days)))
  ;;    total)
  (let* ((now (current-time))
	 (todays-date (timeclock-time-to-date now))
	 (first t) (accum 0) (elapsed 0)
	 event beg last-date
	 last-date-limited last-date-seconds)
    (when (or (not timeclock-discrepancy)
              ;; The length of the workday has changed, so recompute.
              (not (equal timeclock-workday timeclock--previous-workday)))
      (when (file-readable-p timeclock-file)
	(setq timeclock-project-list nil
	      timeclock-last-project nil
	      timeclock-reason-list nil
	      timeclock-elapsed 0)
	(with-temp-buffer
	  (insert-file-contents timeclock-file)
	  (goto-char (point-max))
	  (unless (re-search-backward "^b\\s-+" nil t)
	    (goto-char (point-min)))
	  (while (setq event (timeclock-read-moment))
	    (cond ((equal (car event) "b")
		   (setq accum (string-to-number (nth 2 event))))
		  ((equal (car event) "h")
		   (setq last-date-limited
			 (timeclock-time-to-date (cadr event))
			 last-date-seconds
			 (* (string-to-number (nth 2 event)) 3600.0)))
		  ((equal (car event) "i")
		   (when (and (nth 2 event)
			      (> (length (nth 2 event)) 0))
		     (add-to-list 'timeclock-project-list (nth 2 event))
		     (setq timeclock-last-project (nth 2 event)))
		   (let ((date (timeclock-time-to-date (cadr event))))
		     (if (if last-date
			     (not (equal date last-date))
			   first)
			 (setq first nil
			       accum (- accum (if last-date-limited
						  last-date-seconds
						timeclock-workday))))
		     (setq last-date date
			   last-date-limited nil)
		     (if beg
			 (error "Error in format of timelog file!")
		       (setq beg (cadr event)))))
		  ((equal (downcase (car event)) "o")
		   (if (and (nth 2 event)
			    (> (length (nth 2 event)) 0))
		       (add-to-list 'timeclock-reason-list (nth 2 event)))
		   (if (not beg)
		       (error "Error in format of timelog file!")
		     (setq timeclock-last-period
			   (float-time (time-subtract (cadr event) beg))
			   accum (+ timeclock-last-period accum)
			   beg nil))
		   (if (equal last-date todays-date)
		       (setq timeclock-elapsed
			     (+ timeclock-last-period timeclock-elapsed)))))
	    (setq timeclock-last-event event
		  timeclock-last-event-workday
		  (if (equal todays-date last-date-limited)
		      last-date-seconds
		    timeclock-workday))
	    (forward-line))
	  (setq timeclock-discrepancy accum
                timeclock--previous-workday timeclock-workday))))
    (unless timeclock-last-event-workday
      (setq timeclock-last-event-workday timeclock-workday))
    (setq accum (or timeclock-discrepancy 0)
	  elapsed (or timeclock-elapsed elapsed))
    (if timeclock-last-event
	(if (equal (car timeclock-last-event) "i")
	    (let ((last-period (timeclock-last-period now)))
	      (setq accum (+ accum last-period)
		    elapsed (+ elapsed last-period)))
	  (if (not (equal (timeclock-time-to-date
			   (cadr timeclock-last-event))
			  (timeclock-time-to-date now)))
	      (setq accum (- accum timeclock-last-event-workday)))))
    (list accum (- elapsed timeclock-last-event-workday)
	  elapsed)))

;;; A reporting function that uses timeclock-log-data

(defun timeclock-day-base (&optional time)
  "Given a time within a day, return 0:0:0 within that day.
If optional argument TIME is non-nil, use that instead of the current time."
  (let ((decoded (decode-time time)))
    (setf (decoded-time-second decoded) 0)
    (setf (decoded-time-minute decoded) 0)
    (setf (decoded-time-hour decoded) 0)
    (encode-time decoded)))

(defun timeclock-mean (l)
  "Compute the arithmetic mean of the values in the list L."
  (if (not (consp l))
      0
    (let ((total 0))
      (dolist (thisl l)
        (setq total (+ total thisl)))
      (/ total (length l)))))

(defun timeclock-generate-report (&optional html-p)
  "Generate a summary report based on the current timelog file.
By default, the report is in plain text, but if the optional argument
HTML-P is non-nil, HTML markup is added."
  (interactive "P")
  (let ((log (timeclock-log-data))
	(today (timeclock-day-base)))
    (if html-p (insert "<p>"))
    (insert "Currently ")
    (let ((project (nth 2 timeclock-last-event))
	  (begin (nth 1 timeclock-last-event))
	  done)
      (if (timeclock-currently-in-p)
	  (insert "IN")
	(if (zerop (length project))
	    (progn (insert "Done Working Today")
		   (setq done t))
	  (insert "OUT")))
      (unless done
	(insert " since " (format-time-string "%Y/%m/%d %-I:%M %p" begin))
	(if html-p
	    (insert "<br>\n<b>")
	  (insert "\n*"))
	(if (timeclock-currently-in-p)
	    (insert "Working on "))
	(if html-p
	    (insert project "</b><br>\n")
	  (insert project "*\n"))
	(let ((proj-data (cdr (assoc project (timeclock-project-alist log))))
	      (two-weeks-ago (time-subtract today (* 2 7 24 60 60)))
	      two-week-len today-len)
	  (while proj-data
	    (if (not (time-less-p
		      (timeclock-entry-begin (car proj-data)) today))
		(setq today-len (timeclock-entry-list-length proj-data)
		      proj-data nil)
	      (if (and (null two-week-len)
		       (not (time-less-p
			     (timeclock-entry-begin (car proj-data))
			     two-weeks-ago)))
		  (setq two-week-len (timeclock-entry-list-length proj-data)))
	      (setq proj-data (cdr proj-data))))
	  (if (null two-week-len)
	      (setq two-week-len today-len))
	  (if html-p (insert "<p>"))
	  (if today-len
	      (insert "\nTime spent on this task today: "
		      (timeclock-seconds-to-string today-len)
		      ".  In the last two weeks: "
		      (timeclock-seconds-to-string two-week-len))
	    (if two-week-len
		(insert "\nTime spent on this task in the last two weeks: "
			(timeclock-seconds-to-string two-week-len))))
	  (if html-p (insert "<br>"))
	  (insert "\n"
		  (timeclock-seconds-to-string (timeclock-workday-elapsed))
		  " worked today, "
		  (timeclock-seconds-to-string (timeclock-workday-remaining))
		  " remaining, done at "
		  (timeclock-when-to-leave-string) "\n")))
      (if html-p (insert "<p>"))
      (insert "\nThere have been "
	      (number-to-string
	       (length (timeclock-day-alist log)))
	      " days of activity, starting "
	      (caar (last (timeclock-day-alist log))))
      (if html-p (insert "</p>"))
      (when html-p
	(insert "<p>
<table>
<td width=\"25\"><br></td><td>
<table border=1 cellpadding=3>
<tr><th><i>Statistics</i></th>
    <th>Entire</th>
    <th>-30 days</th>
    <th>-3 mons</th>
    <th>-6 mons</th>
    <th>-1 year</th>
</tr>")
	(let* ((day-list (timeclock-day-list))
	       (thirty-days-ago (time-subtract today (* 30 24 60 60)))
	       (three-months-ago (time-subtract today (* 90 24 60 60)))
	       (six-months-ago (time-subtract today (* 180 24 60 60)))
	       (one-year-ago (time-subtract today (* 365 24 60 60)))
	       (time-in  (vector (list t) (list t) (list t) (list t) (list t)))
	       (time-out (vector (list t) (list t) (list t) (list t) (list t)))
	       (breaks   (vector (list t) (list t) (list t) (list t) (list t)))
	       (workday  (vector (list t) (list t) (list t) (list t) (list t)))
	       (lengths  (vector 0 thirty-days-ago three-months-ago
				 six-months-ago one-year-ago)))
	  ;; collect statistics from complete timelog
	  (dolist (day day-list)
	    (dotimes (i 5)
	      (unless (time-less-p
		       (timeclock-day-begin day)
		       (aref lengths i))
		(let ((base (timeclock-day-base (timeclock-day-begin day))))
		  (nconc (aref time-in i)
			 (list (float-time (time-subtract
					    (timeclock-day-begin day)
				            base))))
		  (let ((span (timeclock-day-span day))
			(len (timeclock-day-length day))
			(req (timeclock-day-required day)))
		    ;; If the day's actual work length is less than
		    ;; 70% of its span, then likely the exit time
		    ;; and break amount are not worthwhile adding to
		    ;; the statistic
		    (when (and (> span 0)
			       (> (/ (float len) (float span)) 0.70))
		      (nconc (aref time-out i)
			     (list (float-time (time-subtract
						(timeclock-day-end day)
						base))))
		      (nconc (aref breaks i) (list (- span len))))
		    (if req
			(setq len (+ len (- timeclock-workday req))))
		    (nconc (aref workday i) (list len)))))))
	  ;; average statistics
	  (dotimes (i 5)
	    (aset time-in i (timeclock-mean (cdr (aref time-in i))))
	    (aset time-out i (timeclock-mean (cdr (aref time-out i))))
	    (aset breaks i (timeclock-mean (cdr (aref breaks i))))
	    (aset workday i (timeclock-mean (cdr (aref workday i)))))
	  ;; Output the HTML table
	  (insert "<tr>\n")
	  (insert "<td align=\"center\">Time in</td>\n")
	  (dotimes (i 5)
	    (insert "<td align=\"right\">"
		    (timeclock-seconds-to-string (aref time-in i))
		    "</td>\n"))
	  (insert "</tr>\n")

	  (insert "<tr>\n")
	  (insert "<td align=\"center\">Time out</td>\n")
	  (dotimes (i 5)
	    (insert "<td align=\"right\">"
		    (timeclock-seconds-to-string (aref time-out i))
		    "</td>\n"))
	  (insert "</tr>\n")

	  (insert "<tr>\n")
	  (insert "<td align=\"center\">Break</td>\n")
	  (dotimes (i 5)
	    (insert "<td align=\"right\">"
		    (timeclock-seconds-to-string (aref breaks i))
		    "</td>\n"))
	  (insert "</tr>\n")

	  (insert "<tr>\n")
	  (insert "<td align=\"center\">Workday</td>\n")
	  (dotimes (i 5)
	    (insert "<td align=\"right\">"
		    (timeclock-seconds-to-string (aref workday i))
		    "</td>\n"))
	  (insert "</tr>\n"))
	(insert "<tfoot>
<td colspan=\"6\" align=\"center\">
  <i>These are approximate figures</i></td>
</tfoot>
</table>
</td></table>")))))

;;; A helpful little function

(defun timeclock-visit-timelog ()
  "Open the file named by `timeclock-file' in another window."
  (interactive)
  (find-file-other-window timeclock-file))

;;; Modern time tracking implementation

;; The following code is an attempt to bring typical features from
;; modern time tracking systems to timeclock.

(defconst timeclock-time-regexp
  (rx (repeat 2 digit) ":" (repeat 2 digit) ":" (repeat 2 digit))
  "Regexp matching a timestamp on the form HH:MM:SS.")

(defconst timeclock-date-regexp
  (rx (repeat 4 digit) "/" (repeat 2 digit) "/" (repeat 2 digit))
  "Regexp matching a date on the form YYYY/MM/DD.")

(defcustom timeclock-excluded-items nil
  "List storing items never counted for statistic generation."
  :type '(repeat :tag "Excluded items" (string :tag "Item")))

(defvar timeclock--items-internal nil)

(defvar timeclock-time-input-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map minibuffer-local-map)
    (define-key map (kbd "SPC") #'timeclock-input-time-separator)
    map)
  "Keymap used for inputting a time on timeclock.")

(defface timeclock-time
  '((t (:inherit bold)))
  "Face used for timestamps in timelog file.")

(defface timeclock-date
  '((t (:inherit font-lock-constant-face)))
  "Face used for dates in timelog file.")

(defface timeclock-tag
  '((t (:inherit font-lock-keyword-face)))
  "Face used for tags and other separators in timelog file.")

(defface timeclock-heading
  '((t :inherit font-lock-function-name-face))
  "Face used for the heading of timeclock report buffers.")

(defvar timelog-font-lock-keywords
  (list (list timeclock-time-regexp '(0 'timeclock-time))
        (list timeclock-date-regexp '(0 'timeclock-date))
        (list "\\[" '(0 'timeclock-tag))
        (list "\\]" '(0 'timeclock-tag))
        (list "^#.*" '(0 'font-lock-comment-face)))
  "Keywords for syntax highlighting on timelog file.")

(defvar timeclock-report-font-lock-keywords
  (list (list timeclock-separator '(0 'timeclock-tag))
        (list "\\[" '(0 'timeclock-tag))
        (list "\\]" '(0 'timeclock-tag))
        (list "^This .*:" '(0 'timeclock-heading))
        (list "^Date:  .*" '(0 'timeclock-heading))
        (list "^Period:  .*" '(0 'timeclock-heading))
        (list "\\(remaining\\) for reaching goal\\]$" '(1 'warning))
        (list "Goal \\(reached\\)\\]$" '(1 'success))
        (list "^\\(Total\\): " '(1 'underline))
        (list "^\\(Average\\): " '(1 'underline))
        (list "^\\(Most time .*\\): " '(1 'underline))
        (list "^\\(Less time .*\\): " '(1 'underline))
        (list "^\\(Tags report\\):$" '(1 'underline))
        (list "^\\(Pending goals\\):$" '(1 'underline))
        ;; (list (concat (string ett-graph-icon) "+") '(0 'ett-separator-face))
        )
  "Keywords for syntax highlighting on timeclock report buffers.")

(defun timeclock-new-item-below (&optional tag)
  "Create a new timelog track below current one.
The user will be prompted for a date, time and item.
Date will default to date of current line.
If TAG is non-nil (interactively, with a prefix argument),
also ask for a tag."
  (interactive "P")
  (let (this-date item)
    (save-excursion
      (beginning-of-line)
      (re-search-forward timeclock-date-regexp nil t)
      (setq this-date
            (buffer-substring-no-properties (match-beginning 0) (point))))
    (end-of-line)
    (insert
     "\n  "
     (completing-read (format-prompt "Date" this-date) nil nil nil nil nil this-date)
     " "
     (let ((time (read-from-minibuffer "Time: " nil timeclock-time-input-map)))
       (if (= (length time) 5)
           (concat time ":00")
         time))
     " "
     (setq item (timeclock--complete "Item: " 'items))
     (or (when tag (concat " [" (timeclock--complete "Tag: " item) "]")) ""))))

(defun timeclock--complete (prompt set)
  "Ask for user input with PROMPT for SET.
PROMPT must be a string.
SET can be the symbol `items' or an item for tag search."
  (let (olmap)
    ;; This is a hack; SPC can be used on tags, so we override
    ;; minibuffer keymap here
    (setq olmap minibuffer-local-completion-map)
    (define-key minibuffer-local-completion-map " " nil)
    (prog1
        (completing-read
         prompt
         (cond ((eq set 'items) (timeclock--return-items))
               (t
                (with-temp-buffer
                  (insert-file-contents timeclock-file)
                  (timeclock--parse-tags set))
                (mapcar
                 #'car (assoc-delete-all "No tags" timeclock--items-internal)))))
      ;; Restore keymap
      (setq minibuffer-local-completion-map olmap))))

(defun timeclock--return-items ()
  "Return a list with all items on timelog file."
  (with-temp-buffer
    (insert-file-contents timeclock-file)
    (timeclock--parse-items-list)
    ;; Return value
    timeclock--items-internal))

(defun timeclock--item-at-point ()
  "Return buffer substring between point and end of line or start of tag."
  (buffer-substring-no-properties
   (point)
   (save-excursion
     (if (search-forward "[" (line-end-position) t)
         (progn (forward-char -2) (point))
       (end-of-line)
       (point)))))

(defun timeclock--time-at-point (&optional nolimit)
  "Return time at point as a list of hours and minutes.
When NOLIMIT is non-nil, don't stick with HH:MM; rather,
allow HHH:MM and others."
  (let ((point (point))
        hr mn)
    (setq hr (string-to-number
              (if nolimit
                  (buffer-substring-no-properties
                   point
                   (progn (skip-chars-forward "0-9") (point)))
                (buffer-substring-no-properties point (+ point 2))))
          mn (string-to-number
              (if nolimit
                  (buffer-substring-no-properties
                   (+ (point) 1)
                   (+ (point) 3))
                (buffer-substring-no-properties (+ point 3) (+ point 5)))))
    ;; We are done with this timestamp, so go to end of line
    (end-of-line)
    ;; Return time
    (list hr mn)))

(defun timeclock--time-difference (time1 time2)
  "Calculate time difference between TIME1 and TIME2.
These both must be lists on the form (HOURS MINUTES)."
  (let (hr mn)
    ;; Calculate difference between hours and minutes
    (setq hr (- (nth 0 time2) (nth 0 time1))
          mn (- (nth 1 time2) (nth 1 time1)))
    ;; If hours are negative, add 24
    (if (> 0 hr) (setq hr (+ 24 hr)))
    ;; If minutes are negative, add 60 and substract an hour
    (if (> 0 mn)
        (setq mn (+ 60 mn)
              hr (- hr 1)))
    ;; Now convert to minutes only and return that
    (while (> hr 0)
      (setq mn (+ 60 mn)
            hr (- hr 1)))
    mn))

(defun timeclock-prettify-time (time)
  "Return a prettified string for TIME."
  (let ((hr (floor time 60))
        (mn (% time 60)))
    (concat
     (if (> hr 0) (format "%dh " hr) "")
     (format "%dm" (or mn 0)))))

(defun timeclock--convert-to-iso (time)
  "Convert TIME into a YYYY-MM-DD date string."
  (replace-regexp-in-string "/" "-" time))

(defun timeclock--prepare-scope (date)
  "Clean everything from point to point-min.
When there's a previous track, don't delete it, and change
timestamp to 00:00:00 and date to DATE."
  (let (beacon)
    (beginning-of-line)
    (unless (bobp)
      (re-search-backward timeclock-time-regexp nil t)
      (skip-chars-forward "0-9:")
      (setq beacon (point))
      (beginning-of-line)
      (delete-region (point) beacon)
      (insert "i " date " 00:00:00"))
    (beginning-of-line)
    (delete-region (point) (point-min))))

(defun timeclock--finalize-scope (&optional timestamp)
  "Add a dumb track at the end of current buffer with current time.
This allows calculating times accurately even for unfinished tracks.
When TIMESTAMP is non-nil, use that as timestamp."
  (catch 'exit
    (goto-char (point-max))
    (save-excursion
      ;; Maybe the last item is a clock-out.  Let's check that.
      (re-search-backward "^[[:alnum:] ]")
      (when (looking-at "[o|O]") (throw 'exit nil)))
    (insert
     "\no 9999/12/31 "
     (or timestamp (format-time-string "%H:%M:%S"))
     " UglyDumbItem")))

(defun timeclock--custom-date (date &optional date2)
  "Operate on current buffer so the only date is DATE.
With optional DATE2, include every date between DATE and DATE2."
  (if (not (stringp date)) (error "Date not defined"))
  (goto-char (point-min))
  (save-excursion
    (unless (or (search-forward date nil t) date2)  ; TODO: create a converter
      (error "Date not found"))
    (timeclock--prepare-scope date)
    (if (stringp date2)
        (when (search-forward date2 nil t)
          ;; This takes us to the last one
          (while (search-forward date2 nil t) t)
          (forward-line 1)
          (delete-region (point) (point-max))
          (timeclock--finalize-scope "00:00:00"))
      (delete-non-matching-lines date (point-min) (point-max))
      (timeclock--finalize-scope "00:00:00"))))

(defun timeclock-prepare-parse ()
  "Delete all lines starting with 'b', 'h' or '#'.
'b' and 'h' are used internally by timeclock and are never real
tracks.
'#' is the chosen syntax for comments."
  (delete-matching-lines "^[b|h|#]" (point-min) (point-max)))

(defun timeclock--parse-buffer (&optional all)
  "Parse every timeclock item on current buffer.
When ALL is non-nil, also parse items on `timeclock-excluded-items'.
WARNING: This function (and any other with 'parse' in the name)
can modify the buffer from which it was called on."
  (let (ctime ntime item total onlist)
    (save-excursion
      (setq timeclock--items-internal nil)
      (timeclock-prepare-parse)
      (goto-char (point-min))
      (while (re-search-forward timeclock-time-regexp nil t)
        ;; There's at least one item to parse, and we are at the right line.
        ;; But wait!  Maybe this is just a clock out message.  We preserve
        ;; backward compatibility by assuming an 'o' (or 'O') at the start of a
        ;; line means "This item is just a silly reminder last clock finished,
        ;; thank you"
        (if (save-excursion (progn (beginning-of-line) (looking-at "[o|O]")))
            (setq item "Untracked")
          ;; If there isn't an 'o' or 'O', we are good.  This is a valid track.
          ;; We jump over any whitespaces here and collect item name.
          (save-excursion
            (skip-chars-forward " ")
            (setq item (timeclock--item-at-point))))
        ;; We now want to collect times.
        ;; As per specifications, we are at it, but in the wrong end.
        (skip-chars-backward "0-9:")
        ;; We name this time 'ctime' (current time).
        (setq ctime (timeclock--time-at-point))
        ;; We have our first time. Go to next timestamp.
        ;; We save excursion, as we want to come back here later.
        (save-excursion
          (if (not (re-search-forward timeclock-time-regexp nil t))
              ;; If there's no next time, we'll say 'ntime' and 'ctime' are
              ;; equal.  This is a smart hack, as time is 0 if there's no
              ;; difference.
              (setq ntime ctime)
            ;; There's a next time and we are at it.
            ;; We don't care what item this is, just collect time.
            (skip-chars-backward "0-9:")
            ;; We name this time 'ntime' (next time).
            (setq ntime (timeclock--time-at-point))))
        ;; Now, we calculate how many time there is.
        (when (or all (not (member item timeclock-excluded-items)))
          (setq total (timeclock--time-difference ctime ntime)
                onlist (alist-get item timeclock--items-internal nil nil 'equal))
          ;; Add it to our list
          (if onlist
              ;; Increment value on list if already there
              (setf (alist-get item timeclock--items-internal nil nil 'equal)
                    (+ onlist total))
            ;; Add it otherwise
            (setq timeclock--items-internal
                  (append (list (cons item total)) timeclock--items-internal))))))))

(defun timeclock-parse-item (item)
  "Parse current buffer so total time for ITEM is collected."
  (let (ctime ntime total onlist item-reg)
    (save-excursion
      (setq timeclock--items-internal nil
            item-reg (concat timeclock-time-regexp " " (regexp-quote item)))
      (timeclock-prepare-parse)
      (goto-char (point-min))
      (while (save-excursion (re-search-forward item-reg nil t))
        (catch 'invalid
          ;; There's at least one track to parse, so jump to it.
          (re-search-forward item-reg)
          ;; But wait!  What if this is not really an item?
          (when (save-excursion
                  (progn (beginning-of-line) (looking-at "[o|O]")))
            (throw 'invalid nil))
          ;; We are good now.
          ;; We only want to collect times.
          ;; Fortunately, match-beginning points there.
          (goto-char (match-beginning 0))
          ;; We name this time 'ctime' (current time).
          (setq ctime (timeclock--time-at-point))
          ;; We have our first time. Go to next timestamp.
          ;; We save excursion, as we want to come back here later.
          (save-excursion
            (re-search-forward timeclock-time-regexp nil t)
            ;; We don't care about what item this is. Just collect time.
            (goto-char (match-beginning 0))
            ;; We name this time 'ntime' (next time).
            (setq ntime (timeclock--time-at-point)))
          ;; Now, we calculate how many time there is.
          (setq total (timeclock--time-difference ctime ntime)
                onlist (alist-get item timeclock--items-internal nil nil 'equal))
          ;; Add it to our list
          (if onlist
              ;; Increment value on list if already there
              (setf (alist-get item timeclock--items-internal nil nil 'equal)
                    (+ onlist total))
            ;; Add it otherwise
            (setq timeclock--items-internal
                  (append
                   (list (cons item total)) timeclock--items-internal))))))))

(defun timeclock--parse-tags (item)
  "Parse current buffer so tag values for ITEM are collected."
  (let (ctime ntime tag total onlist item-reg beacon)
    (save-excursion
      (setq timeclock--items-internal nil
            item-reg (concat timeclock-time-regexp " " (regexp-quote item)))
      (timeclock-prepare-parse)
      (goto-char (point-min))
      (while (save-excursion (re-search-forward item-reg nil t))
        ;; There's at least one item to parse, so jump to it.
        (re-search-forward item-reg)
        ;; The start of this block is relevant.  Save it.
        (setq beacon (match-beginning 0))
        ;; We now collect tag.
        ;; As per specifications, whitespace(s) separes us from it.
        (skip-chars-forward " ")
        ;; Set tag.
        (if (not (looking-at "\\["))
            ;; We aren't looking at a tag. Use a "No tags" tag.
            (setq tag "No tags")
          ;; We are looking at a tag. Set it.
          (forward-char)
          (setq tag
                (buffer-substring-no-properties
                 (point)
                 (save-excursion
                   (search-forward "]" (line-end-position))
                   (forward-char -1) (point)))))
        ;; We now want to collect times.
        ;; Go back to beacon.
        (goto-char beacon)
        ;; We name this time 'ctime' (current time).
        (setq ctime (timeclock--time-at-point))
        ;; We have our first time. Go to next timestamp.
        ;; We save excursion, as we want to come back here later.
        (save-excursion
          (if (not (re-search-forward timeclock-time-regexp nil t))
              ;; If there's no next time, we'll say 'ntime' and 'ctime' are
              ;; equal.
              (setq ntime ctime)
            (re-search-forward timeclock-time-regexp nil t)
            ;; We don't care about what item this is. Just collect time.
            (goto-char (match-beginning 0))
            ;; We name this time 'ntime' (next time).
            (setq ntime (timeclock--time-at-point)))
          ;; Now, we calculate how many time there is.
          (setq total (timeclock--time-difference ctime ntime)
                onlist (alist-get tag timeclock--items-internal nil nil 'equal))
          ;; Add it to our list
          (if onlist
              ;; Increment value on list if already there
              (setf (alist-get tag timeclock--items-internal nil nil 'equal)
                    (+ onlist total))
            ;; Add it otherwise
            (setq timeclock--items-internal
                  (append (list (cons tag total)) timeclock--items-internal))))))))

(defun timeclock--parse-items-list ()
  "Parse current buffer so all timelog items are collected."
  (let (item)
    (save-excursion
      (setq timeclock--items-internal nil)
      (timeclock-prepare-parse)
      (goto-char (point-min))
      (while (re-search-forward timeclock-time-regexp nil t)
        (catch 'invalid
          ;; There's at least one item to parse.
          ;; But wait!  This can be a simple comment.
          (when (save-excursion
                  (progn (beginning-of-line) (looking-at "[o|O]")))
            (throw 'invalid nil))
          ;; We are good now.
          ;; As per specifications, whitespace(s) separes us from item.
          (skip-chars-forward " ")
          ;; Set item.
          (setq item (timeclock--item-at-point))
          ;; We don't care about times, we just want an item list.
          ;; So, store it right away.
          (unless (member item timeclock--items-internal)
            (setq timeclock--items-internal (cons item timeclock--items-internal))))))))

(defun timeclock--helper (string &optional error-message time1 time2 dumbtime)
  "Prepare current buffer for timeclock statistic generation.
STRING is the first date we should track. This is passed to
`format-time-string', so it can have placeholders such as %y
for current year.
With ERROR-MESSAGE, signal an error when STRING is not found. By
default, this function silently ignores that scenario.
Optional TIME1 and TIME2 changes how placeholders are replaced.
For instance, if TIME1 is the symbol :day and TIME2 is the
integer -7, %d in STRING will be replaced by the day that was current
exactly one week ago. These arguments are passed to `make-decoded-time'.
DUMBTIME is passed to `timeclock--finalize-scope'."
  (goto-char (point-min))
  (let (desired-date date)
    (save-excursion
      (when (and time1 time2)
        (let* ((today (decode-time))
               (last-date-1
                (decoded-time-add
                 today (funcall #'make-decoded-time time1 time2)))
               (last-date (encode-time last-date-1)))
          (setq desired-date last-date)))
      (setq date (format-time-string string desired-date))
      (cond ((search-forward date nil t)
             (timeclock--prepare-scope date)
             (timeclock--finalize-scope dumbtime))
            (error-message (error error-message))
            (t (timeclock--finalize-scope dumbtime))))))

(defun timeclock-api (item &optional date1 date2)
  "Return timeclock total time for ITEM.
DATE1 and DATE2, which are strings on the form YYYY/MM/DD, can be
used to delimit scope.
DATE1 can also be a symbol such as day, month, week or year."
  (catch 'exit
    (with-temp-buffer
      (insert-file-contents timeclock-file)
      (when (stringp date1)
        (condition-case nil (timeclock--custom-date date1 date2)
          (t (throw 'exit 0))))
      (when (and (symbolp date1) (not (null date1)))
        (pcase date1
          ('day
           (timeclock--helper
            "%Y/%m/%d"
            "Error: current day wasn't found; maybe call timeclock-clock-in?"))
          ('month (timeclock--helper "%Y/%m/01"))
          ('year (timeclock--helper "%Y/01/01"))
          ('week (timeclock--helper "%Y/%m/%d" nil :day -6))
          (_ (throw 'exit 0))))
      (timeclock-parse-item item)
      (or (alist-get item timeclock--items-internal nil nil 'equal) 0))))

(defun timeclock--insert-time (item)
  "Write collected times for ITEM on current buffer."
  (let ((time (alist-get item timeclock--items-internal nil nil #'equal))
        hr mn)
    (unless (= time 0)
      ;; Convert time to strings for hours and minutes
      (setq hr (number-to-string (floor time 60))
            mn (number-to-string (% time 60)))
      ;; Insert item and time
      (insert item " " timeclock-separator " "
              (if (length= hr 1) "0" "")
              hr
              (if (length= mn 2) ":" ":0")
              mn "\n"))))

(defun timeclock--sort ()
  "Sort all tracks on current buffer, and align items."
  (let (indent-tabs-mode)
    (goto-char (point-min))
    (save-excursion
      ;; Handle three digits tracks
      (when (re-search-forward " [0-9]\\{3\\}:" nil t)
        (goto-char (point-min))
        (while (re-search-forward " [0-9]\\{2\\}:" nil t)
          (goto-char (match-beginning 0))
          (forward-char)
          (insert "0")
          (end-of-line))))
    (save-excursion
      ;; Sort items
      ;; We need to do it in two steps because it's somehow complicated
      (sort-regexp-fields t "^.*$" "\\(:[0-9]+\\)" (point-min) (point-max))
      (sort-regexp-fields t "^.*$" "\\([0-9]+:\\)" (point-min) (point-max))
      ;; We now align
      (align-regexp
       (point-min) (point-max) (concat "\\(\\s-*\\)" timeclock-separator)))
    (save-excursion
      ;; We now have aligned and cute items
      ;; Except for those trailing zeros...
      (while (re-search-forward " 0[0-9]\\{2\\}:" nil t)
        (goto-char (match-beginning 0))
        (forward-char)
        (delete-char 1)
        (insert " ")
        (end-of-line)))))

(defun timeclock--calculate-percentage (&optional nototal)
  "Add a percentage value next to each track.
When NOTOTAL is non-nil, don't add total indicator."
  (let ((total 0))
    (goto-char (point-min))
    (save-excursion
      (while (re-search-forward "[0-9]+:" nil t)
        (goto-char (match-beginning 0))
        (let ((num (timeclock--time-at-point t)))
          (setq total (+ (nth 1 num) (* (nth 0 num) 60) total)))))
    (save-excursion
      (while (re-search-forward "[0-9]+:" nil t)
        (goto-char (match-beginning 0))
        (let ((num (timeclock--time-at-point t))
              percentage)
          (setq percentage
                (number-to-string
                 (round (* 100
                           (/
                            (float (+ (* (nth 0 num) 60) (nth 1 num)))
                            total)))))
          (insert
           "   [" (if (length= percentage 1) " " "") percentage "%]"))))
    (goto-char (point-min))
    (while (search-forward "[ 0%]" nil t)
      (replace-match "[<1%]" nil nil))
    (unless nototal
      (goto-char (point-max))
      (insert "\n\nTotal: " (timeclock-prettify-time total)))))

;;;###autoload
(defun timeclock-statistics-report (scope &optional item finaldate)
  "Report statistics for SCOPE.
SCOPE can be a symbol (day, month, year or week) or
a custom date as a string (e.g. 2023/02/03).
With ITEM, report only statistics for ITEM.
With FINALDATE, use it to delimit SCOPE."
  (interactive (list 'day))
  (let ((inhibit-read-only t))
    (with-current-buffer (get-buffer-create "*timeclock-report*")
      (erase-buffer)
      ;; First, we get the contents from our file
      (insert-file-contents timeclock-file)
      ;; For changing the scope of our tracking, we
      ;; just erase what we don't need
      (pcase scope
        ('day
         (timeclock--helper
          "%Y/%m/%d"
          "Error: current day wasn't found; maybe call timeclock-clock-in?"))
        ('month (timeclock--helper "%Y/%m/01"))
        ('year (timeclock--helper "%Y/01/01"))
        ('week (timeclock--helper "%Y/%m/%d" nil :day -6))
        (_ (timeclock--custom-date scope finaldate)))
      ;; We now parse our buffer
      (if item
          (timeclock--parse-tags item)
        (timeclock--parse-buffer))
      ;; We no longer need whatever is here
      (delete-region (point-min) (point-max))
      ;; Now we write our values
      (mapc #'timeclock--insert-time (mapcar #'car timeclock--items-internal))
      ;; Prepare view
      (timeclock--sort)
      (insert
       (cond ((eq scope 'day)
              "This day:")
             ((eq scope 'week)
              "This week:")
             ((eq scope 'month)
              "This month:")
             ((eq scope 'year)
              "This year:")
             (finaldate
              (format "Period:  %s -- %s" scope finaldate))
             (t (format "Date:  %s" scope)))
       "\n\n")
      ;; Add percentage
      (timeclock--calculate-percentage item)
      ;; TODO: Add statistics (graph, etc.)
      ;; (when item (ett-add-complex-statistics item scope finaldate))
      ;; TODO: Add goal indication
      ;; (when (and ett-goals (not item)) (ett-add-goals scope))
      ;; TODO: Add icons
      ;; (ett-add-icon item)
      )
    ;; Finish
    (switch-to-buffer "*timeclock-report*")
    (timeclock-report-mode)
    (read-only-mode)
    (goto-char (point-min))))

(defun timeclock-report-year ()
  "Report statistics for current year."
  (interactive)
  (timeclock-statistics-report 'year))

(defun timeclock-report-month ()
  "Report statistics for current month."
  (interactive)
  (timeclock-statistics-report 'month))

(defun timeclock-report-week ()
  "Report statistics for current week."
  (interactive)
  (timeclock-statistics-report 'week))

(defun timeclock-report-period ()
  "Report statistics for a period."
  (interactive)
  (timeclock-choose-custom-date t))

(transient-define-prefix timeclock-choose-view ()
  "Choose desired view for report."
  ["Available views:\n"
   [("y" "Year" timeclock-report-year)
    ("m" "Month" timeclock-report-month)
    ("w" "Week" timeclock-report-week)
    ("d" "Day" timeclock-statistics-report)
    ("c" "Custom date" timeclock-choose-custom-date)
    ("p" "Custom period" timeclock-report-period)
    ;; ("i" "Detailed stats for item" timeclock-report-item)
    ]])

(defun timeclock-choose-custom-date (&optional period)
  "Let the user choose a custom date, or period if PERIOD is non-nil."
  (interactive)
  (save-window-excursion
    (calendar)
    (if (not period)
        (timeclock-statistics-report (read-string "Date (YYYY/MM/DD): "))
      (timeclock-statistics-report
       (read-string "Date 1 (YYYY/MM/DD): ") nil
       (read-string "Date 2 (YYYY/MM/DD): ")))))

(defun timeclock-input-time-separator ()
  "Input a time separator (colon)."
  (interactive)
  (insert ":"))

(defvar-keymap timelog-mode-map
  "C-c C-c" #'timeclock-in
  "M-RET"   #'timeclock-new-item-below)

(define-derived-mode timelog-mode text-mode "Timelog"
  "Major mode for viewing and editing timelog files.
Timelog files are generated by timeclock package."
  (setq font-lock-defaults '(timelog-font-lock-keywords t)))

(defvar-keymap timeclock-report-mode-map
  "q"      #'bury-buffer
  "<up>"   #'scroll-down-command
  "<down>" #'scroll-up-command
  "SPC"    #'timeclock-choose-view)

(define-derived-mode timeclock-report-mode timelog-mode "Timeclock report"
  "Major mode for viewing timeclock reports."
  (setq font-lock-defaults '(timeclock-report-font-lock-keywords t))
  (put-text-property 1 (point-max) 'line-prefix "   ")
  (setq-local view-read-only nil)
  (setq cursor-type nil))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\timelog\\'" . timelog-mode))

(provide 'timeclock)

(run-hooks 'timeclock-load-hook)

;; make sure we know the list of reasons, projects, and have computed
;; the last event and current discrepancy.
(if (file-readable-p timeclock-file)
    ;; FIXME: Loading a file should not have these kinds of side-effects.
    (timeclock-reread-log))

;;; timeclock.el ends here
